
from django.test import TestCase,Client
from django.urls import resolve
from .views import article_list,article_add,article_page,article_delete
from .models import Article
from .forms import *

class TestModel(TestCase):
    def test_if_article_model_exist(self):
        Client().post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        hitung_jumlah_data = Article.objects.all().count()
        self.assertEquals(hitung_jumlah_data, 1)
class TestForm(TestCase):
    def test_if_article_form_is_valid(self):
        articleForm = ArticleForm(data={'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        self.assertTrue(articleForm.is_valid())
    def test_if_article_form_is_invalid(self):
        articleForm = ArticleForm(data={})
        self.assertFalse(articleForm.is_valid())
class TestURL(TestCase):
    def test_main_url_is_exist (self) :
        response = Client().get('/article_list/')
        self.assertEqual(response.status_code, 200)
        response = Client().get('/add/')
        self.assertEqual(response.status_code, 200)
    def test_article_page_url_is_exist(self):
        contohArtikel = Client().post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        response = Client().get('/article/1/', {}, True)
        self.assertEquals(response.status_code, 200)
class TestView(TestCase):
    def test_if_article_list_template_exists_on_the_page(self):
        response = Client().get('/article_list/')
        self.assertTemplateUsed(response, 'article/landing.html')
    def test_if_article_add_template_exists_on_the_page(self):
        response = Client().get('/add/')
        self.assertTemplateUsed(response, 'article/form.html')
    def test_if_article_page_template_exists_on_the_page(self):
        Client().post('/add/',{'artikel' : 'kosong','penulis' : 'siapa ya','deskripsi' : 'isi artikel'})
        response = Client().get('/article/1/',{},True)
        self.assertTemplateUsed(response, 'article/view.html')


