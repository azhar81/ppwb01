from django.urls import path
from . import views
app_name = 'article'
urlpatterns = [
    path('article_list/', views.article_list, name='article_list'),
    path('article/<int:id>/', views.article_page, name='article_page'),
    path('add/', views.article_add, name='article_add'),
    path('artikel/<int:id>/delete/', views.article_delete, name='article_delete')
]
