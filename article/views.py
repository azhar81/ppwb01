from django.shortcuts import render, get_object_or_404, redirect
from .models import Article
from .forms import ArticleForm
from komentar.forms import KomentarForm

# Create your views here.
def article_list(request):
    artikels = Article.objects.all()
    return render(request, 'article/landing.html', {'artikels':artikels})

def article_page(request, id):
    artikel = get_object_or_404(Article, id=id)
    list_komentar=artikel.komentar_set.all()

    response = {
        'artikel':artikel,
        'list_komentar':list_komentar,
    }

    if request.method == "POST":
        form = KomentarForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.artikel = artikel
            comment.save()
            return redirect('article:article_page', id=artikel.id)
    else:
        form = KomentarForm()
        response['form']=form

    return render(request, 'article/view.html', response)

def article_add(request):
    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            artikel = form.save()
            artikel.save()
            return redirect('article:article_page', id=artikel.id)
    else:
        form = ArticleForm()
        artikel = Article.objects.all()
        context = {'form':form,'artikel':artikel}
        return render(request, 'article/form.html', context)

def article_delete(request, id):
    Article.objects.get(id=id).delete()
    return redirect('article:article_list')

