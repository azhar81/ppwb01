from django.test import TestCase, Client, LiveServerTestCase, tag
from django.urls import resolve
from .forms import *
from .models import *
from selenium import webdriver

# Create your tests here.
class TestKomentar(TestCase):
    def test_komentar_dapat_dibuat(self):
        # pembuatan artikel
        test_article = Article(artikel='test_artikel', penulis='test_penulis', deskripsi='test_deskripsi')
        test_article.save()

        # membuat komentar
        test_komentar = Komentar(artikel=test_article, commenter='test_commenter', comment='test_comment')
        test_komentar.save()

        self.assertEqual(test_article.komentar_set.all().count(), 1)
    
    def test_komentar_form_is_valid(self):
        form = KomentarForm(data={'commenter':'test_commenter', 'comment':'test comment'})
        self.assertTrue(form.is_valid())

    def test_komentar_form_is_not_valid_1(self):
        form = KomentarForm(data={'commenter':'test_comment_dengan_panjang_lebih_dari_20', 'comment':'test comment'})
        self.assertFalse(form.is_valid())

        form = KomentarForm(data={'comment':'test comment'})
        self.assertFalse(form.is_valid())

        form = KomentarForm(data={'commenter':'test'})
        self.assertFalse(form.is_valid())

    def test_komentar_muncul_pada_halaman(self):
        # pembuatan artikel
        test_article = Article(artikel='test_artikel', penulis='test_penulis', deskripsi='test_deskripsi')
        test_article.save()

        # post komentar
        Client().post('/add_comment/1/', {'commenter':'test_commenter', 'comment':'test_comment'})
        
        response = Client().get('/article/1/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn('test_commenter', html_kembalian)
        self.assertIn('test_comment', html_kembalian)

    # def test_komentar_bisa_hapus_komentar(self):
    #     # pembuatan artikel
    #     test_article = Article(artikel='test_artikel', penulis='test_penulis', deskripsi='test_deskripsi')
    #     test_article.save()

    #     # membuat komentar
    #     test_komentar = Komentar(artikel=test_article, commenter='test_commenter', comment='test_comment')
    #     test_komentar.save()
        
    #     self.assertEqual(test_article.komentar_set.all().count(), 1)

    #     response = Client().get('delete_comment/1/1/')
    #     self.assertEqual(test_article.komentar_set.all().count(), 0)