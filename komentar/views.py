from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *

# Create your views here.
def add_comment(request, id):
    artikel = get_object_or_404(Article, id=id)

    if request.method == "POST":
        form = KomentarForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.artikel = artikel
            comment.save()
            return redirect('article:article_page', id=id)
    else:
        
        response['form'] = KomentarForm()

    return render(request, 'article/view.html', response)

def delete_comment(request, id_artikel, id_komentar):
    Komentar.objects.get(id=id_komentar).delete()
    return redirect('article:article_page', id=id_artikel)