from django.urls import path
from . import views

app_name = 'komentar'
urlpatterns = [
    path('add_comment/<int:id>/', views.add_comment, name='add_comment'),
    path('delete_comment/<int:id_artikel>/<int:id_komentar>', views.delete_comment, name='delete_comment')
]
