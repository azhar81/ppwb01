from django.db import models
from django.utils import timezone
from article.models import Article

# Create your models here.
class Komentar(models.Model):
    commenter = models.CharField(max_length=20, default='Anonymous')
    comment = models.TextField()
    waktu = models.DateTimeField(default=timezone.now)
    artikel = models.ForeignKey(Article, on_delete=models.CASCADE, null=True)