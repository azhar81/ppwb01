from django.urls import re_path

from . import views

app_name = 'relawan'

urlpatterns = [
    re_path(r'^relawan', views.relawan, name='relawan'),
    re_path('formrelawan', views.formRelawan, name='formrelawan'),
    re_path('saverelawan', views.saverelawan),
]