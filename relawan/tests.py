from django.test import TestCase, Client
from .models import RelawanList
from .forms import Input_Form

# Create your tests here.
class TestingRelawan(TestCase):
	def test_apakah_url_relawan_ada(self):
		response = Client().get('/relawan/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_relawan_ada_templatenya(self):
		response = Client().get('/relawan/')
		self.assertTemplateUsed(response, 'main/relawan.html')

	def test_apakah_url_form_relawan_ada(self):
		response = Client().get('/formrelawan/')
		self.assertEqual(response.status_code, 200)

	def test_apakah_form_relawan_ada_templatenya(self):
		response = Client().get('/formrelawan/')
		self.assertTemplateUsed(response, 'main/formRelawan.html')

	def test_apakah_sudah_ada_model_form(self):
		RelawanList.objects.create(display_name="Ini Nama", alamat="ini alamat", donasi="300000")
		hitung_banyak_relawan = RelawanList.objects.all().count()
		self.assertEqual(hitung_banyak_relawan, 1)

	def test_apakah_di_halaman_form_sudah_menyimpan_data_dan_ditampilkan(self):
		response = Client().post('/saverelawan/', {'display_name'	: 'Ini Nama', 'alamat':'ini alamat', 'donasi' : '300000'})
		self.assertEquals(response.status_code, 302)

	def test_form(self):
		form = Input_Form()
		self.assertIn('id="id_display_name"', form.as_p())
		self.assertIn('id="id_alamat', form.as_p())
		self.assertIn('id="id_donasi', form.as_p())
