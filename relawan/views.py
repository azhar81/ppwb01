from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import RelawanList

# Create your views here.
def relawan(request):
	relawans = RelawanList.objects.all()
	response = {'relawans' : relawans}
	return render(request, 'main/relawan.html', response)


def formRelawan(request):
	response = 	{'input_form' : Input_Form}
	return render(request, 'main/formRelawan.html', response)

def saverelawan(request):
	form = Input_Form(request.POST)
	if request.method == 'POST':
		if form.is_valid():
				form.save()
				return HttpResponseRedirect('/relawan')
		else:
				return HttpResponseRedirect('/formrelawan')