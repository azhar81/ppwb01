from django.shortcuts import render
import requests

def home(request):
    response = requests.get('https://api.kawalcorona.com/indonesia/provinsi')
    data = response.json()
    kasus = []
    for o in data:
        datanya = {}
        datanya['provinsi'] = o['attributes']['Provinsi']
        datanya['kasus_positif'] = o['attributes']['Kasus_Posi']
        datanya['kasus_sembuh'] = o['attributes']['Kasus_Semb']
        datanya['kasus_meninggal'] = o['attributes']['Kasus_Meni']
        kasus.append(datanya)
    return render(request, 'main/home.html', {
        'data': kasus,
    })